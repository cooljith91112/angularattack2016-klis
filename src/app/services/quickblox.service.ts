import { Injectable } from '@angular/core';
import {Config} from '../config'
import {Observable} from 'rxjs/Observable';

declare var QB:any;

@Injectable()
export class QuickBloxService{
    constructor(){
        QB.init(Config.contsants.quickBloxAppID, Config.contsants.quickBloxAuthKey, Config.contsants.quickBloxAuthSecret);

    }
    
    init(){
        
       var data =  QB.createSession(function(err,result){
            console.log('Session create callback', err, result);
             return Promise.resolve(result);
        });
      
      var returnData = new Observable(observer => data);
      return returnData;
        
    }
    
    checkUserExists(userid:string){
        
        let params:any = {
            external: userid
        };
        let response:any;
        QB.users.get(params, function(err, result){
            if (result) {
                response = result;
            } else  {
                response = err;
            }
        });
        
       return Promise.resolve(response);
    }
    
}