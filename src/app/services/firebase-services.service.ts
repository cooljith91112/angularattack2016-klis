import { Injectable } from '@angular/core';
import {AngularFire, FirebaseAuth, FirebaseAuthState} from 'angularfire2';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class FirebaseServicesService {

  constructor(public angularFire:AngularFire,public auth: FirebaseAuth) {
    
  }
  
  isAuthenticated():Boolean{
    if(this.auth.getAuth())
    {
      return true;
    }
    return false;
  }
  
  fireBaseLogin():Promise<FirebaseAuthState>{
    return this.angularFire.auth.login();
  }
  
  getAuthUser():any{
    return this.auth.getAuth();
  }
  
  fireBaseLogout():any{
    return this.angularFire.auth.logout();
  }
  
  getAngularFireInstance():AngularFire{
    return this.angularFire;
  }

}
