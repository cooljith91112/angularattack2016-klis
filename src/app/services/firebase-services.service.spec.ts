import {
  beforeEachProviders,
  it,
  describe,
  expect,
  inject
} from '@angular/core/testing';
import { FirebaseServicesService } from './firebase-services.service';

describe('FirebaseServices Service', () => {
  beforeEachProviders(() => [FirebaseServicesService]);

  it('should ...',
      inject([FirebaseServicesService], (service: FirebaseServicesService) => {
    expect(service).toBeTruthy();
  }));
});
