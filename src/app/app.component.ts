import { Component, OnInit, EventEmitter } from '@angular/core';
import {Router, Routes, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router';
import {AngularFire, FirebaseAuth} from 'angularfire2';
import { Login } from './login/login.component';
import {Home} from './home/home.component'
import {Chat} from './chat/chat.component'
import {Observable} from 'rxjs/Observable';
import {FirebaseServicesService} from './services/firebase-services.service'


@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives:[ROUTER_DIRECTIVES],
  providers:[FirebaseServicesService]
})

@Routes([
  {path:'/',component: Home},
  {path:'#/chat',component: Chat}
])


export class AppComponent implements OnInit{
  title= 'Easy App works!';
  isLoggedin = false;
  
  eventEmitter:EventEmitter<any> = new EventEmitter();
  
  constructor( public router:Router , public firebaseService:FirebaseServicesService){}
  
  ngOnInit(){    
    if(this.firebaseService.isAuthenticated()){
      this.router.navigate(['#/chat']);   
      this.isLoggedin = true;
      this.eventEmitter.emit(this.isLoggedin);
    }
  }
  
  login(){
    this.firebaseService.fireBaseLogin().then(items =>{ 
      // console.log(items)
      this.router.navigate(['#/chat']);
      this.isLoggedin = true;
      this.eventEmitter.emit(this.isLoggedin);
    });

  }
  
  logout(){
    this.firebaseService.fireBaseLogout();
    this.router.navigate(['/']);
    this.isLoggedin = true;
    this.eventEmitter.emit(this.isLoggedin);
  }
  
}

