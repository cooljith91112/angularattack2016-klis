import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { AppComponent } from './app.component';

beforeEachProviders(() => [AppComponent]);

describe('App: Angularattack2016Klis', () => {
  it('should create the app',
      inject([AppComponent], (app: AppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'angularattack2016-klis works!\'',
      inject([AppComponent], (app: AppComponent) => {
    expect(app.title).toEqual('angularattack2016-klis works!');
  }));
});
