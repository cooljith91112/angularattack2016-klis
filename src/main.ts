import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppComponent, environment } from './app/';
import {ROUTER_PROVIDERS} from '@angular/router';
import {HTTP_PROVIDERS} from '@angular/http';
import {Config} from './app/config'
import {FIREBASE_PROVIDERS, defaultFirebase, AuthProviders, AuthMethods, firebaseAuthConfig} from 'angularfire2';
if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent,[
  HTTP_PROVIDERS,
  ROUTER_PROVIDERS,
  FIREBASE_PROVIDERS,
  defaultFirebase(Config.contsants.firebaseUrl),
  firebaseAuthConfig({provider: AuthProviders.Github, method:AuthMethods.Popup})
]);
