import { Angularattack2016KlisPage } from './app.po';

describe('angularattack2016-klis App', function() {
  let page: Angularattack2016KlisPage;

  beforeEach(() => {
    page = new Angularattack2016KlisPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('angularattack2016-klis works!');
  });
});
