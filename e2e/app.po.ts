export class Angularattack2016KlisPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('angularattack2016-klis-app h1')).getText();
  }
}
